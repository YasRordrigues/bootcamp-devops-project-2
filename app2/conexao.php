<?php
$servername = "mysql";
$username = "root";
$password = "password";
$database = "meubanco";

// Criar conexão
$link = new mysqli($servername, $username, $password, $database);

// Verificar conexão
if ($link->connect_error) {
    die("Connection failed: " . $link->connect_error);
}

// Verificar se a tabela 'mensagens' existe e criar se não existir
$createTableQuery = "
CREATE TABLE IF NOT EXISTS mensagens (
    id INT PRIMARY KEY,
    nome VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    mensagem TEXT NOT NULL
)";

if ($link->query($createTableQuery) !== TRUE) {
    die("Error creating table: " . $link->error);
}
?>
